# Package

version       = "0.0.1"
author        = "Michael Riley"
description   = "A project to aggregate and report on bank transaction exports"
license       = "MIT"

bin = @["monies_comp"]
srcDir = "src"
binDir = "bin"
skipExt = @["nim"]

# Dependencies

requires "nim >= 0.17.2"
