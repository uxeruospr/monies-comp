import os, parsecsv, streams, strutils

proc loadFile*(dataFile: string, hasHeader: bool = false): CsvParser =

  var file = newFileStream(dataFile, fmRead)

  if file == nil:
    quit("Cannot open data file")

  var parser: CsvParser
  open(parser, file, dataFile, ',', '\"')

  if hasHeader:
    readHeaderRow(parser)

  return parser

proc writeOutFile*(fileName: string, data: string): void =

  writeFile("out/" & fileName, data)
