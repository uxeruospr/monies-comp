import os, parsecsv, streams, times, sequtils, strutils, nre
import data_io, data_types

proc parseTransactionType(trans: string): char =
  # CR -> C
  # DR -> D
  return trans[0]

proc parseTransactionMerchant(desc: var string): string =

  let toStrip: array[2, string] = [
    "([0-9]{3}-?){2}[0-9]{4}",
    "((Debit Card)|(ATM)|(POS)|(ACH)|(Check)|(Fee)|(Pmt)|(Shared Branch)) W\\/D.*"
  ]

  for i in 0 .. high(toStrip):
    desc = desc.replace(re(toStrip[i]), "")

  return strip(desc)

proc loadCategories(): seq[string] =

  var data = loadFile("data/categories.csv")
  var cats = newSeq[string]()

  # add placeholder for 0th position
  cats.add("")
  
  while readRow(data):
    cats.add(data.row[0])

  close(data)

  return cats

proc lookupCategories(cats: seq[string], lCats: seq[int]): seq[string] =

  var rCats = newSeq[string]()

  for i in lCats:
    rCats.add(cats[i])

  return rCats

proc lookupMerchantCategories(lookupCats: seq[int]): seq[string] =

  let cats = loadCategories()
  var stringCats = newSeq[string]()

  for i in lookupCats:
    stringCats.add(cats[i])

  return stringCats

proc splitCategoriesToInt(cats: string): seq[int] =

    var keys = newSeq[int]()

    if len(cats) != 0:
      for n in split(cats, {';'}):
        keys.add(parseInt(n))

    return keys

proc lookupMerchant(merchants: seq[MerchantData], lookupDesc: string): MerchantData =

  for merch in merchants:
    if merch.description == lookupDesc:
      return merch

  # TODO: handle missing merchant

proc loadMerchantData*(): seq[MerchantData] =

  var data = loadFile("data/merchant_metadata.csv")
  var merchants = newSeq[MerchantData]()

  while readRow(data):

    var merchName = ""
    let merchDesc = data.row[0]

    if len(data.row[1]) > 0:
      merchName = data.row[1]
    else:
      merchName = data.row[0]

    var merchCat = newSeq[string]()

    merchCat = lookupMerchantCategories(splitCategoriesToInt(data.row[2]))

    var merch: MerchantData = (
      description: merchDesc,
      name: merchName,
      categories: merchCat
    )

    merchants.add(merch)
  
  close(data)

  return merchants

proc parseTransactions*(): seq[TransactionRecord] =

  var parser = loadFile("data/data.csv", true)

  var recs = newSeq[TransactionRecord]()
  let categories = loadCategories()
  let merchants = loadMerchantData()

  while readRow(parser):

    let amts = split(parser.row[4], '.')
    let desc = parseTransactionMerchant(parser.row[3])

    var rec: TransactionRecord = (
      account: strip(parser.row[0]),
      transType: parseTransactionType(parser.row[5]),
      date: parse(parser.row[1], "MM/dd/yy"),
      dollarAmount: parseInt(amts[0]),
      centAmount: parseInt(amts[1]),
      merchant: lookupMerchant(merchants, desc)
    )

    recs.add(rec)

  close(parser)

  return recs
