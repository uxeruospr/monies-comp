import os, sequtils, strutils
import data_types

proc getUniqueMerchants*(transactions: seq[TransactionRecord]): seq[string] =

  var merchants = newSeq[string]()

  for i in 0 .. high(transactions):
    if transactions[i].merchant.name == "":
      continue

    if transactions[i].transType == 'D':
      if find(merchants, transactions[i].merchant.name) == -1:
        merchants.add(transactions[i].merchant.name)

  return merchants

proc findTransactionsByMerchant*(merchant: string): seq[TransactionRecord] =

  return newSeq[TransactionRecord]()
