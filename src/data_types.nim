import times

type
  TransactionRecord* = tuple 
    account: string
    transType: char 
    date: TimeInfo
    dollarAmount: int 
    centAmount: int
    merchant: MerchantData

  MerchantData* = tuple
    description: string
    name: string
    categories: seq[string]
